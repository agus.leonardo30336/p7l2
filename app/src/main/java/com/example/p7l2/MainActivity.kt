package com.example.p7l2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    class MainActivity : AppCompatActivity(),MainVPInterface {

        override fun tampilLuas(model: MainModel) {
            result.text = model.luas.toString()
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            fullCycle.isChecked =true
            var presenter = MainPresenter(this)
            var pil: Int = 1
            radioGroup?.setOnCheckedChangeListener { group, checkedId ->
                pil =
                        if (R.id.fullCycle == checkedId) 1
                        else if (R.id.threeQuartersCycle == checkedId) 2
                        else if (R.id.halfCycle == checkedId) 3
                        else 4
            }
            proces.setOnClickListener {
                val radioGroup = findViewById<RadioGroup>(R.id.radioGroup)
                var jari2 = r.text.toString().trim()
                var isOk = true;
                if(jari2.isEmpty()){
                    isOk = false
                    r.setError("Tidak boleh kosong")
                }
                if(isOk){
                    var convertJari2 = jari2.toDouble()
                    presenter.hitungLuas(convertJari2, pil)
                }
                else{
                    Toast.makeText(this,"Error Input",
                        Toast.LENGTH_SHORT).show()
                }
            }
        }
    }


}
