package com.example.p7l2

class MainPresenter(setView :MainVPInterface) {
    private val view = setView
    fun luas(jari2 : Double, pil:Int) : Double {
        return when (pil) {
            1 -> 22.0 / 7.0 * jari2 * jari2
            2 -> 3.0/4.0 * 22.0 / 7.0 * jari2 * jari2
            3 -> 1.0/2.0 * 22.0 / 7.0 * jari2 * jari2
            else -> 1.0/4.0 * 22.0 / 7.0 * jari2 * jari2
        }
    }
    fun hitungLuas(jari2 : Double, pil:Int){
        val hasil = luas(jari2,pil)
        val mainModel = MainModel(hasil)
        view.tampilLuas(mainModel);
    }
}

