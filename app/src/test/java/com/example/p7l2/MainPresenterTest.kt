package com.example.p7l2

import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {
    @Test
    fun hitungLuas() {
        val view : MainVPInterface =
            mock(MainVPInterface::class.java)
        val presenter = MainPresenter(view)
        val luas = presenter.luas(14.0,1)
        assertEquals(616.0,luas,0.00001)
    }
}

